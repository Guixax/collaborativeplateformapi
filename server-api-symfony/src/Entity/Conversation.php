<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConversationRepository")
 */
class Conversation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"conversation"})
     */

    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="conversations")
     * @Groups({"conversation"})
     */
    private $author;

    /**
     * @ORM\Column(type="text")
     * @Groups({"conversation", "user"})
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"user", "conversation"})
     */
    private $dateSent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getDateSent(): ?\DateTimeInterface
    {
        return $this->dateSent;
    }

    public function setDateSent(\DateTimeInterface $dateSent): self
    {
        $this->dateSent = $dateSent;

        return $this;
    }
}
