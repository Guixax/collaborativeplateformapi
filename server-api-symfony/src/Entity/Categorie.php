<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
 */
class Categorie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $catImage;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\File", inversedBy="categories")
     */
    private $fileCat;

    public function __construct()
    {
        $this->fileCat = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCatImage(): ?string
    {
        return $this->catImage;
    }

    public function setCatImage(?string $catImage): self
    {
        $this->catImage = $catImage;

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFileCat(): Collection
    {
        return $this->fileCat;
    }

    public function addFileCat(File $fileCat): self
    {
        if (!$this->fileCat->contains($fileCat)) {
            $this->fileCat[] = $fileCat;
        }

        return $this;
    }

    public function removeFileCat(File $fileCat): self
    {
        if ($this->fileCat->contains($fileCat)) {
            $this->fileCat->removeElement($fileCat);
        }

        return $this;
    }

}
