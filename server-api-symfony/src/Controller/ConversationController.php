<?php

namespace App\Controller;

use App\Entity\Conversation;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ConversationController extends AbstractController
{

    /**
     * @Route("/conversations", name="conversation_get", methods={"GET","HEAD"})
     */
    public function conversationGet(SerializerInterface $serializer)
    {

        $conversations = $this->getDoctrine()->getRepository(Conversation::class)->findAll();
        $conversationCount = count($conversations);
        $jsonData = $serializer->normalize($conversations, 'json', ['groups' => 'conversation']);

        return $this->json([
            "count" => $conversationCount,
            "result" => $jsonData
        ]);
    }

     /**
     * @Route("/conversations", name="conversation_add", methods={"POST"})
     */
    public function conversationAdd(Request $request, EntityManagerInterface $em, SerializerInterface $serializer)
    {
        //retrieve requests json data and returns an array 
        $data = json_decode($request->getContent(), true);

        //denormalize the array to a Genre object
        $conversations = $serializer->denormalize(
            $data,
            Conversation::class,
            'json'
        );

        //persist this entity via doctrine
        $em->persist($conversations);
        $em->flush();

        return $this->json(
            [
                "id" => $conversations->getId(),
                "url" => $this->generateUrl(
                    'conversation_view',
                    [
                        "id" => $conversations->getId()
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )            ]
        );
    }
    

    /**
     * @Route("/conversations/{id}", name="conversation_view", methods={"GET","POST"})
     */

    public function conversationView(int $id)
    {
       $conversations = $this->getDoctrine()->getRepository(Conversation::class)->find($id);

       if ($conversations){
           return $this->json($conversations);
       } else {
           $this->json([
               "error_message" => "no conversation were found with this id  "] , 404);
       
       }
    }

    /**
     * @Route("/conversations/{id}", name="conversation_delete", methods={"DELETE"}, requirements={
     *  "id" = "\d+"
     * })
     */
    public function conversationDelete(int $id, EntityManagerInterface $em)
    {
        $conversations = $this->getDoctrine()->getRepository(Conversation::class)->find($id);

        if ($conversations){
            $em->remove($conversations);
            $em->flush();

            return $this->json([
                "message" => "Conversation successfully deleted"
            ]);
        } else {
            return $this->json([
                "error_message" => "No conversation found with this id"
            ], 404);
        }
    }

     /**
     * @Route("/conversations/{id}", name="conversation_edit", methods={"PUT"}, requirements={
     *  "id" = "\d+"
     * })
     */
    public function conversationEdit(int $id, Request $request, EntityManagerInterface $em, SerializerInterface $serializer)
    {


        $conversations = $this->getDoctrine()->getRepository(Conversation::class)->find($id);

        if ($conversations) {
            $data = json_decode($request->getContent(), true);

            $conversations = $serializer->denormalize(
                $data,
                Conversation::class,
                'json',
                ['object_to_populate' => $conversations]
            );

            $em->persist($conversations);
            $em->flush();

            return $this->json($conversations);
        } else {
            return $this->json([
                "error_message" => "No conversation found with this id"
            ], 404);
        }
    }

}
