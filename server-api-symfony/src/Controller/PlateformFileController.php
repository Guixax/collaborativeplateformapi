<?php

namespace App\Controller;

use App\Entity\File;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PlateformFileController extends AbstractController
{
    /**
     * @Route("/plateformFiles", name="files_get", methods={"GET","HEAD"})
     */
    public function filesGet()
    {
        $files= $this->getDoctrine()->getRepository(File::class)->findAll();
        $fileCount= count($files);

        return $this->json( ["count" => $fileCount,
                            "result" => $files]);
    }

     /**
     * @Route("/plateformFiles", name="files_add", methods={"POST"})
     */
    public function filesAdd(Request $request, EntityManagerInterface $em)
    {
        $data = json_decode($request -> getContent(), true);

        $normalizer = new ObjectNormalizer();
        $file = $normalizer->denormalize($data, File::class, 'json');

        $em->persist($file);
        $em->flush();

            return $this->json([
                "id" => $file->getId(),
                "url" => $this->generateUrl(
                    'files_view',
                    [
                        "id" => $file->getId()
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                "data" => $file
            ], 201);

    }

    /**
     * @Route("/plateformFiles/{id}", name="files_view", methods={"GET","POST"})
     */

    public function filesView(int $id)
    {
       $file = $this->getDoctrine()->getRepository(File::class)->find($id);

       if ($file){
           return $this->json($file);
       } else {
           $this->json([
               "error_message" => "no file were found with this id  "] , 404);
       
       }
    }

    /**
     * @Route("/plateformFiles/{id}", name="files_delete", methods={"DELETE"}, requirements={
     *  "id" = "\d+"
     * })
     */
    public function filesDelete($id, EntityManagerInterface $em)
    {
        $file = $this->getDoctrine()->getRepository(File::class)->find($id);

        if ($file){
            $em->remove($file);
            $em->flush();

            return $this->json([
                "message" => sprintf("File '%s' successfully deleted", $file->getName())
            ]);
        } else {
            return $this->json([
                "error_message" => "No file found with this id"
            ], 404);
        }
    }

     /**
     * @Route("/plateformFiles/{id}", name="files_edit", methods={"PUT"}, requirements={
     *  "id" = "\d+"
     * })
     */
    public function filesEdit(int $id, Request $request, EntityManagerInterface $em)
    {


        $file = $this->getDoctrine()->getRepository(File::class)->find($id);
        
        if ($file){
            $data = json_decode($request->getContent(), true);
            
            $normalizer = new ObjectNormalizer();
            
            $file = $normalizer->denormalize($data, File::class, 'json', ['object_to_populate' => $file ]);

            $em->persist($file);
            $em->flush();

            return $this->json($file);

         } else {
            return $this->json([
                "error_message" => "No file found with this id"
            ], 404);
         }


     }

}
