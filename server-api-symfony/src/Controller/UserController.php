<?php

namespace App\Controller;

use App\Entity\Conversation;
use App\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;
class UserController extends AbstractController
{




/**
     * @Route("/users", name="users_get", methods={"GET","HEAD"})
     */
    public function userGet(SerializerInterface $serializer)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findAll();
        $userCount = count($user);
        
        $jsondata = $serializer->normalize($user, 'json', ['groups' => 'user']);

        return $this->json([
            "count" => $userCount,
            "result" => $jsondata
        ]);
    }

    /**
    * @Route("/users/{id}", name="users_view", methods={"GET","HEAD"}, requirements={
    *  "id" = "\d+"
    * })
    */
    function userView(int $id, SerializerInterface $serializer)
    {
       $user = $this->getDoctrine()->getRepository(User::class)->find($id);

       $jsondata = $serializer->normalize($user, 'json', ['groups' => 'user']);

    
       if ($user) {
           return $this->json($jsondata);
       } else {
           return $this->json([
               "error_message" => "No user were found with this id"
           ], 404);
       }
    }

    /**
     * @Route("/users", name="users_add", methods={"POST"})
     */
    public function usersAdd(Request $request, EntityManagerInterface $em, SerializerInterface $serializer)
    {

        //retrieve requests json data and returns an array 
        $data = json_decode($request->getContent(), true);
        
        //denormalize the array to a Genre object
        $user = $serializer->denormalize(
            $data,
            User::class,
            'json',
        );

        //persist this entity via doctrine
        $em->persist($user);
        $em->flush();

        return $this->json(
            [
                "id" => $user->getId(),
                "url" => $this->generateUrl(
                    'users_view',
                    [
                        "id" => $user->getId()
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                "data" => $user
            ]
        );
    }


    /**
     * @Route("/users/{id}", name="users_delete", methods={"DELETE"}, requirements={
     *  "id" = "\d+"
     * })
     */
    public function usersDelete($id, EntityManagerInterface $em)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if ($user) {
            $em->remove($user);
            $em->flush();

            return $this->json([
                "message" => sprintf("user '%s' successfully deleted", $user->getUsername())
            ]);
        } else {
            return $this->json([
                "error_message" => "No user found with this id"
            ], 404);
        }
    }

    /**
     * @Route("/users/{id}", name="users_edit", methods={"PUT"}, requirements={
     *  "id" = "\d+"
     * })
     */
    public function usersEdit(int $id, EntityManagerInterface $em, Request $request, SerializerInterface $serializer)
    {

        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if ($user) {
            $data = json_decode($request->getContent(), true);

            $user = $serializer->denormalize(
                $data,
                User::class,
                'json',
                ['object_to_populate' => $user]
            );

            $em->persist($user);
            $em->flush();

            return $this->json($user);
        } else {
            return $this->json([
                "error_message" => "No user found with this id"
            ], 404);
        }
    }

    /**
     * @Route("/users/{id}", name="users_conversation", methods={"GET"}, requirements={
     *  "id" = "\d+"
     * })
     */
    public function usersConversation(int $id)
    {

        $user = $this->getDoctrine()->getRepository(Conversation::class)->findall();

        if ($user) {
            $data = json_decode($request->getContent(), true);

            $data['dob'] = new DateTime($data['dob']);

            $user = $serializer->denormalize(
                $data,
                User::class,
                'json',
                ['object_to_populate' => $user]
            );

            $em->persist($user);
            $em->flush();

            return $this->json($user);
        } else {
            return $this->json([
                "error_message" => "No user found with this id"
            ], 404);
        }
    }

}