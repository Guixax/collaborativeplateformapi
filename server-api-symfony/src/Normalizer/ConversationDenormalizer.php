<?php

namespace App\Normalizer;

use App\Entity\Conversation;
use Doctrine\ORM\EntityManagerInterface;
use Proxies\__CG__\App\Entity\User;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ConversationDenormalizer implements ContextAwareDenormalizerInterface{

    private $entityManager;
    private $objectNormalizer;

    public function __construct(EntityManagerInterface $em, ObjectNormalizer $on)
    {
        $this->entityManager = $em;
        $this->objectNormalizer = $on;
    }

    public function supportsDenormalization($data, $type, $format = null, array $context = [])
    {
        return $type == Conversation::class;
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        $author = $this->entityManager->find(User::class, $data['author']);

        \array_filter($data, function($key){
            return $key !== 'author';
        });
        
        $conversation = $this->objectNormalizer->denormalize($data, $type, $format, $context);
        if ($author){
            $conversation->setAuthor($author);
            return $conversation;        
        } else {
            return null;
        }
    }
}