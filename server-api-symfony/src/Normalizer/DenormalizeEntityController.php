<?php

namespace App\Normalizer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;

class DenormalizeEntityController implements ContextAwareDenormalizerInterface

{
    private $entityManager;

    public function __construct(EntityManagerInterface $em)
    {
        //getting the entitymanger for later
        $this->entityManager = $em;
    }

    public function supportsDenormalization($data, $type, $format = null, $context = [])
    {
        //check that the $type is an Entity managed by Doctrine 
        //if the type string starts with App\\Entity\\ it is an entity
        //we also check that the data format is readible 
        return (strpos($type, 'App\\Entity\\') === 0) && 
        (is_numeric($data) || is_string($data) || (is_array($data) && isset($data['id'])));
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        return $this->entityManager->find($type, $data);
    }
}
